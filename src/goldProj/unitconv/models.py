from django.db import models

# Create your models here.

class convFactor(models.Model):
	conv_name = models.CharField(max_length=50)
	conv_factor = models.DecimalField(max_digits=8, decimal_places=5)
	def __str__(self):
		return self.conv_name + ' -> ' + str(self.conv_factor)
	
