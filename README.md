Notes for the grader:
	First off, in gold's quandl.js there is a formatMoney(number) function that takes a number and converts it into US Currency that was adapted from
the following link: https://stackoverflow.com/questions/4022171/how-do-i-print-currency-format-in-javascript
The conversion factors were adapted from https://www.metric-conversions.org/weight
Inside of unitconv/views.py there is the required init() view that initalizes the data with conversion factors, however there is an added view nuke()
taken from my blog app that gets rid of any conversion factors that were already in the database.
The gold/index page queries quandl for gold data everytime the web page refreshes, solved by using the 'onload' attribute of the body.
