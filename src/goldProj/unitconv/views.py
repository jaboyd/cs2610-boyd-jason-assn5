from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from django.http import JsonResponse
from .models import convFactor

from decimal import Decimal

def convert(request):
    # Make sure all parameters are given in request.
    if 'value' in request.GET and 'from' in request.GET and 'to' in request.GET:
        f = request.GET['from']
        t = request.GET['to']
        v = request.GET['value']
        try:
            v = Decimal(v)
            multFact = convFactor.objects.filter(conv_name=t)[0]
            valResult = multFact.conv_factor * v

            response = {
                'units': t,
                'value': valResult,
                }

        except:
            response = {
                    'error': "Your 'value' parameter wasn't numeric :(",
                }

    else:
        response = {
                'error': 'You must supply all arguments needed for API use',
                }
    return JsonResponse(response)
	
def init(request):
	# initialize database with necessary conversion factors.
	nuke(request)
	
	convFactors = {
		't_oz': 14.5833,
		'oz': 16.0,
		'kg': 0.453,
	}
	
	for fac in convFactors:
		c = convFactor(conv_name=fac, conv_factor=convFactors[fac])
		c.save()
		
	return HttpResponseRedirect(reverse('gold:index'))

	
def nuke(request):
	for b in convFactor.objects.all():
		b.delete()
	return HttpResponseRedirect(reverse('gold:index'))
