//Personal Quandl API Key
let apiKey = 'api_key=1on2tAQMp4vUMsPVxSvN';

var now = new Date();
var before5 = new Date();

//Subtract 5 days from the now date to get data from Quandl
before5.setDate(now.getDate() - 5);

// toISOString() gets the date into the YYYY-MM-DD format and .splice() subtracts the extra time information.
now = now.toISOString().slice(0, 10);
before5 = before5.toISOString().slice(0, 10);

let start = "start_date=" + before5;
	end   = "end_date=" + now;

// Return a parameter string for a GET request from its arguments.
var formParamsString = function() {
	let argsArray = Array.from(arguments);
	let url = argsArray.shift();
	return `${url}?${argsArray.join('&')}`;
}

// global variable to contain the result of our asynchronous request.
var theData;

//Function uses quandl API to get the json response and stores it into global 'theData'.
var fetch_goldPrice = function() {
	let url = formParamsString('https://www.quandl.com/api/v3/datasets/LBMA/GOLD.json', apiKey, start, end);

	console.log(url);

	fetch(url)
		.then( r => r.json() )
		.then( json => {
			theData = json;
		});
}

var calculate_gold = function() {
    //The first possible entry of troy oz price data
	var goldPrice = theData.dataset.data[0][1];
	input_value = document.getElementById('inpWeight').value;

	var resultDiv = document.createElement('div');
	resultDiv.setAttribute('class', 'center')
	
	var resultP = document.createElement('p');
    resultP.setAttribute('id', 'Price');
	resultDiv.appendChild(resultP);

	if (!isNaN(input_value)) {
        var convURL = `http://${location.hostname}:8000/unitconv/convert?from=lbs&to=t_oz&value=${input_value}`;
        
        console.log(convURL);

		var priceWeight;

        fetch(convURL)
            .then( r => r.json() )
            .then( json => {
				convJson = json;
				priceWeight = parseFloat(convJson.value) * goldPrice;	
				resultP.textContent = "Today Your Weight in Gold is Worth " + formatMoney(priceWeight);	
            });

	} else {
		resultP.textContent = "You must enter a numeric value.";
	}

	document.getElementById('GoldBox').appendChild(resultDiv);
}

/*
    Print currency function adapted from Stack Overflow post: 
    https://stackoverflow.com/questions/11489428/how-to-make-vim-paste-from-and-copy-to-systems-clipboard
*/
function formatMoney(number) {
	return number.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
}



